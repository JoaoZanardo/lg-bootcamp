import http from 'http';

http.createServer((req, res) => {
    res.write('<h1>HELLO WORLD</h1>');
    res.end();
}).listen(8080, () => {
    console.log('Running at port: 8080');
});